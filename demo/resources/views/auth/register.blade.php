<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Signin · Laravel</title>
    <meta name="theme-color" content="#7952b3">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

    </style>
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/signin.css') }}" rel="stylesheet">
</head>

<body class="text-center">
    <main class="form-signup">
        <form method="POST" action="{{ route("user.register") }}">
            @csrf
            <img class="mb-4" src="{{ asset('images/logo.png') }}" alt="" width="72" height="62">
            <h1 class="h3 mb-3 fw-normal">Please sign up</h1>
            <div class="form-floating">
                <input type="text" class="form-control" name="name" placeholder="Your name">
                <label for="floatingInput">Name</label>
            </div>
            <div class="form-floating">
                <input type="email" class="form-control" name="email" placeholder="Email address">
                <label for="floatingPassword">Email address</label>
            </div>

            <div class="form-floating">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <label for="floatingInput">Password</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password-confirm">
                <label for="floatingPassword">Password-confirm</label>
            </div>
            <br>
            <div class="checkbox mb-3">
              <p class="text-muted">
                Already have an account? <a href="#" class="text-reset">Sign up</a>.
              </p>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign up</button>
            <p class="mt-5 mb-3 text-muted">----------</p>
        </form>
    </main>
</body>

</html>
