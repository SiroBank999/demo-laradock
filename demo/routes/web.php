<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::post('/login',[UserController::class,"login"])->name("user.login");
Route::get('/logout',[UserController::class,"logout"]);
Route::post('/register',[UserController::class,"register"])->name("user.register");

Route::get('/register', function () {
    return view('auth/register');
});
