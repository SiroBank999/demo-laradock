<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|min:5|max:255|unique:products",
            "price" => "numeric|max:9999|min:0",
            "image" => "string|max:255",
            "amount" => "integer|min:0",
            "status" => "string|min:10|max:255"
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            ""
        ];
    }

     /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
        ];
    }
}
