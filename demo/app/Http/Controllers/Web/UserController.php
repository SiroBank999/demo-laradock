<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\RegisterFormRequest;

class UserController extends Controller
{
  
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']],$request['remember'])){
            return view("welcome");
        }
    }
    public function logout(Request $request)
    {
        if(Auth::logout()){
            return redirect("login");
        }
        return redirect("register");
    }
    public function register(RegisterFormRequest $request)
    {
        $field = $request->all();
        $field["password"] = Hash::make($field["password"]);
        $user = User::create($field);
        Auth::login($user);
        return view("welcome");
    }
}
