<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterFormRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    public function register(RegisterFormRequest $request){
        $field = $request->all();
        $bcrypt = Hash::make($field["password"]);
        $field["password"] = $bcrypt;
        $user = User::create($field);
        $token = $user->createToken("apiToken")->plainTextToken;
        return response()->json([
            "message" => "Register user successfully",
            "user" => $user,
            "token" => $token
        ], 201);

    }

    public function login(Request $request){
        $user = User::where("email",$request['email'])->first();
        if(Hash::check($request["password"],$user->password)){
            $token = $user->createToken("apiToken")->plainTextToken;
            return response()->json([
                "message" => "Login user successfully",
                "user" => $user,
                "token" => $token
            ], 200);
        }
    }
    public function logout(Request $request){
        auth()->user()->tokens()->delete();
        return response()->json([
            "message" => "LOgged out"
        ], 200);
    }
}
